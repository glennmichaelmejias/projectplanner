import 'package:flutter/material.dart';
import 'package:Proyekto/database.dart';
import 'package:Proyekto/scaffolds/createproject.dart';
import 'package:Proyekto/globalvariables.dart';
import 'package:Proyekto/scaffolds/selectedproject.dart';
import 'package:Proyekto/scaffolds/selectedprojecttabs/fluttersound.dart';
import 'package:Proyekto/widgets.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final navKey = new GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    soundinitialize();
    refresh = () {
      setState(() {});
    };
    initDatabase();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navKey,
      title: 'Project Planner',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: maincolor,
        fontFamily: "Product Sans",
        cursorColor: maincolor,
        appBarTheme: AppBarTheme(elevation: 0, color: maincolor),
        iconTheme: IconThemeData(color: maincolor),
      ),
      home: Scaffold(
        drawer: Drawer(),
        appBar: AppBar(
          title: Text("Project Planner"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.sort),
              onPressed: () {
                if(projectorderdesc){
                  projectorderdesc=false;
                  getprojects(database);
                }
                else{
                  projectorderdesc=true;
                  getprojects(database);
  
                }
              },
            )
          ],
        ),
        body: Builder(
          builder: (context2) => Center(
                  child: ListView.builder(
                    padding: EdgeInsets.only(left:15,top:15,right:15,bottom: 5),
                itemCount: dataname.length,
                itemBuilder: (BuildContext context, int index) {
                  return projectslistviewitem(
                      index: index,
                      context: context,
                      refresh: (int selection) {
                        setState(() {
                          currentselectedcategory = selection;
                        });
                      },
                      onsave: (){
                        getprojects(database).whenComplete(() {
                          setState(() {});
                        });
                      },
                      onlistviewtap: (){
                        currenttabindex=0;
                        isdeleting=false;
                        selectedproject(
                            context: context,
                            index: index,
                            refresh: () {
                              setState(() {});
                            });
                      });
                },
              )),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            final context = navKey.currentState.overlay.context;
            createproject(
                context: context,
                refresh: (int selection) {
                  setState(() {
                    currentselectedcategory = selection;
                  });
                },
                ondone: () {
                  getprojects(database).whenComplete(() {
                    setState(() {});
                  });
                });
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      ),
    );
  }
}
