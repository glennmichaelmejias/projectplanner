import 'package:flutter/material.dart';
import 'package:Proyekto/database.dart';
import 'package:Proyekto/globalvariables.dart';
import 'package:Proyekto/widgets.dart';

void createproject(
    {BuildContext context,
    Function refresh,
    Function ondone,
    Function onsave,
    bool isedit = false,
    int editingindex}) {
  FocusNode txtdescriptionfocusnode = new FocusNode();

  final txttitle = TextEditingController();
  final txtdescription = TextEditingController();
  if (isedit) {
    txttitle.text = dataname[editingindex];
    txtdescription.text = datadescription[editingindex];
    currentselectedcategory = int.parse(dataicon[editingindex]);
  }
  Navigator.of(context).push(PageRouteBuilder(
      opaque: false,
      transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
      ) {
        return SlideTransition(
          position: new Tween<Offset>(
            begin: const Offset(0.0, 1.0),
            end: Offset.zero,
          ).animate(CurvedAnimation(parent: animation, curve: Curves.ease)),
          child: child,
        );
      },
      pageBuilder: (BuildContext context, Animation animation,
              Animation secondaryAnimation) =>
          Scaffold(
            appBar: AppBar(
              title: isedit
                  ? Text("Edit project details")
                  : Text("Create new project"),
            ),
            body: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                        left: 15, right: 15, top: 15, bottom: 15),
                    decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(color: Colors.black12, blurRadius: 16)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 20),
                          padding: EdgeInsets.only(left: 10),
                          decoration: BoxDecoration(
                              border: Border(
                                  left: BorderSide(
                                      color: maincolor.withOpacity(0.2)))),
                          child: TextFormField(
                            controller: txttitle,
                            autofocus: true,
                            style: TextStyle(fontSize: 17),
                            decoration:
                                InputDecoration.collapsed(hintText: "Title.."),
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (term) {
                              FocusScope.of(context)
                                  .requestFocus(txtdescriptionfocusnode);
                            },
                          ),
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    left: BorderSide(
                                        color: maincolor.withOpacity(0.2)))),
                            padding: EdgeInsets.only(left: 10),
                            child: TextFormField(
                              controller: txtdescription,
                              autofocus: true,
                              style: TextStyle(fontSize: 17),
                              maxLines: 100,
                              decoration: InputDecoration.collapsed(
                                  hintText: "Description.."),
                              focusNode: txtdescriptionfocusnode,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(color: Colors.white),
                      child: Container(
                        height: 81,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: iconcategory(refresh: refresh),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(color: maincolor),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            if (!isedit) {
                              createnewproject(
                                  name: txttitle.text,
                                  description: txtdescription.text,
                                  icon: currentselectedcategory.toString());
                              ondone();
                            } else {
                              editprojectdetails(
                                  name: txttitle.text,
                                  description: txtdescription.text,
                                  icon: currentselectedcategory.toString(),
                                  editingid: dataid[editingindex]);
                              onsave();
                            }

                            Navigator.of(context).pop();
                          },
                          child: Container(
                            child: Text(
                              isedit ? "SAVE" : "DONE",
                              textAlign: TextAlign.right,
                              style: TextStyle(color: Colors.white),
                            ),
                            width: double.infinity,
                            padding: EdgeInsets.all(15),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )));
}
