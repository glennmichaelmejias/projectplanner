import 'dart:io';

import 'package:flutter/material.dart';
import 'package:Proyekto/database.dart';
import 'package:Proyekto/globalvariables.dart';
import 'package:Proyekto/scaffolds/selectedprojecttabs/fluttersound.dart';

int currentindexplaying = 0;

Widget resourcelistviewitem({int index, String projectid, BuildContext context}) {
  IconData whaticon() {
    IconData temp;
    switch (resourcetype[index]) {
      case "file":
        temp = Icons.insert_drive_file;
        break;
      case "image":
        temp = Icons.image;
        break;
      case "link":
        temp = Icons.link;
        break;
      case "recording":
        temp = Icons.mic;
        break;
      default:
    }
    return temp;
  }

  if (max_duration == slider_current_position) {
    isPlaying = false;
  }

  return Row(
    children: <Widget>[
      Expanded(
        child: Container(
          margin: EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(boxShadow: <BoxShadow>[
            BoxShadow(color: Colors.black12, blurRadius: 16)
          ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.only(left: 0, top: 1, right: 0),
                padding: EdgeInsets.only(left: 20, right: 10),
                child: Row(
                  children: <Widget>[
                    Container(
                        padding:
                            EdgeInsets.only(right: 20, top: 20, bottom: 20),
                        child: Icon(whaticon())),
                    resourcetype[index] == "recording"
                        ? Container(
                            child: IconButton(
                              icon: currentindexplaying == index && isPlaying
                                  ? Icon(Icons.stop)
                                  : Icon(Icons.play_arrow),
                              onPressed: () {
                                if (isPlaying) {
                                  stopPlayer();
                                } else {
                                  currentindexplaying = index;
                                  if (isPlaying) {
                                    stopPlayer();
                                  }

                                  startPlayer(
                                      filename: resourcefilelink[index]);
                                }
                              },
                            ),
                          )
                        : Container(),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          resourcename[index],
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        resourcetype[index] == "recording"
                            ? Slider(
                                value: currentindexplaying == index
                                    ? slider_current_position
                                    : 0,
                                max: max_duration,
                                divisions: 100,
                                onChanged: (value) {
                                  seekToPlayer(value.toInt());
                                },
                              )
                            : Container()
                      ],
                    )),
                    resourcetype[index] == "image"
                        ? Image.file(
                            File.fromUri(Uri.parse(resourcefilelink[index])),
                            height: 100,
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      isdeleting
          ? IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Delete resource?"),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('Yes'),
                            onPressed: () {
                              Navigator.pop(context);
                              deleteresource(projectid: projectid,resourceid: resourceid[index]);
                              // deletestory(projectid: dataid[index],storyid: item.split("<midseparator>")[item.split("<midseparator>").length-1]);
                              refresh();
                            },
                          ),
                          FlatButton(
                            child: Text('No'),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          )
                        ],
                      );
                    });
              },
            )
          : Container()
    ],
  );
}
