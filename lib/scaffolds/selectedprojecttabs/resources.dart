import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:Proyekto/database.dart';
import 'package:Proyekto/globalvariables.dart';
import 'package:Proyekto/scaffolds/selectedprojecttabs/fluttersound.dart';
import 'package:Proyekto/scaffolds/selectedprojecttabs/widgets.dart';

// bool isrecording = false;
bool isaddlink = false;

// bool isplaying = false;

String imagefile;
String filefile;
Future getImage({Function refresh, int index}) async {
  imagefile = null;
  imagefile = await FilePicker.getFilePath(type: FileType.IMAGE);
  await addresource(
      filelink: imagefile,
      name: imagefile.split("/")[imagefile.split("/").length - 1],
      resourcetype: "image",
      projectid: dataid[index]);
      
  refresh();
}

Future getFile({Function refresh, int index}) async {
  filefile = null;
  filefile = await FilePicker.getFilePath(type: FileType.ANY);
  await addresource(
      filelink: filefile,
      name: filefile.split("/")[filefile.split("/").length - 1],
      resourcetype: "file",
      projectid: dataid[index]);
  refresh();
}
final txturl = TextEditingController();
Widget tabresources({BuildContext context, int index, Function refresh}) {
  return Column(
    children: <Widget>[
      Expanded(
        child: Container(
          width: double.infinity,
          child: ListView.builder(
            padding: EdgeInsets.only(left:15,right:15,top:15,bottom: 5),
              itemCount: resourceid.length,
              itemBuilder: (BuildContext context, int index2) {
                return resourcelistviewitem(index: index2, projectid: dataid[index] , context: context);
              }),
        ),
      ),
      Container(
        child: Material(
          // color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.insert_drive_file),
                      Text(
                        "Add file",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  getFile(refresh: refresh,index: index);
                },
              ),
              InkWell(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.photo),
                      Text(
                        "Add photo",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  getImage(refresh: refresh,index: index);
                },
              ),
              InkWell(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.link),
                      Text(
                        "Add link",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  isaddlink = true;
                  refresh();
                },
              ),
              InkWell(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.mic),
                      Text(
                        "Add recording",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  isRecording = true;
                  
                  startRecorder();
                  refresh();
                },
              ),
            ],
          ),
        ),
      ),
      Material(
        color: Colors.grey[200],
        child: isRecording
            ? Container(
                decoration: BoxDecoration(
                    border: Border(top: BorderSide(color: Colors.grey[300]))),
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          currentfilename.split("/")[currentfilename.split("/").length - 1],
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                    ),
                    Expanded(
                        child: Container(
                            alignment: Alignment.center,
                            child: Text(
                              recorderTxt,
                              style: TextStyle(color: Colors.grey),
                            ))),
                    Expanded(
                      child: Container(
                        child: IconButton(
                          icon: Icon(Icons.stop),
                          onPressed: () {
                            isRecording = false;
                            stopRecorder(projectid: dataid[index]);
                            
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : Container(),
      ),
      Material(
          color: Colors.grey[200],
          child: isaddlink
              ? Container(
                  padding: EdgeInsets.only(left: 10),
                  decoration: BoxDecoration(
                      border: Border(top: BorderSide(color: Colors.grey[300]))),
                  width: double.infinity,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          controller: txturl,

                          autofocus: true,
                          onFieldSubmitted: (term) async {
                            await addresource(
                                name: txturl.text,
                                filelink: "",
                                resourcetype: "link",
                                projectid: dataid[index]);

                            isaddlink = false;
                            refresh();
                          },
                          decoration: InputDecoration.collapsed(
                              hintText: "Paste link.."),
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.check),
                        onPressed: () async {
                          await addresource(
                              name: txturl.text,
                              filelink: "",
                              resourcetype: "link",
                              projectid: dataid[index]);
                          isaddlink = false;
                          refresh();
                        },
                      )
                    ],
                  ),
                )
              : Container())
    ],
  );
}
