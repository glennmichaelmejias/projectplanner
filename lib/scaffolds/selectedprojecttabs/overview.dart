import 'package:flutter/material.dart';
import 'package:Proyekto/globalvariables.dart';

Widget taboverview({int index, BuildContext context}) {
  return Scaffold(
    body: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Container(
              // color: Colors.red,
              margin: EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 0),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(boxShadow: <BoxShadow>[
                BoxShadow(color: Colors.black12, blurRadius: 16)
              ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(0),
                    // color: Colors.blue,
                    // height: 189,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          // padding: EdgeInsets.all(20),
                          // color: Colors.green,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              // Text(
                              //   icontext[int.parse(dataicon[index])],
                              //   style: TextStyle(color: Colors.grey),
                              // ),
                              Icon(
                                iconimage[int.parse(dataicon[index])],
                                size: 100,
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(left: 20),
                            // color: Colors.red,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Project title:",
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      dataname[index],
                                      style: TextStyle(
                                        color: maincolor,
                                        fontSize: 18,
                                      ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Date created:",
                                          style: TextStyle(color: Colors.grey)),
                                      Text(datacreated[index],
                                          style:
                                              TextStyle(color: Colors.black54)),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Category:",
                                          style: TextStyle(color: Colors.grey)),
                                      Text(icontext[int.parse(dataicon[index])],
                                          style:
                                              TextStyle(color: Colors.black54)),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      // margin: EdgeInsets.only(left: 0, right: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Description:",
                            style: TextStyle(color: Colors.grey),
                          ),
                          Expanded(
                            child: ListView(
                              children: <Widget>[
                                Text(
                                  datadescription[index],
                                  style: TextStyle(color: Colors.black54),
                                  textAlign: TextAlign.justify,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 100,
            alignment: Alignment.center,
            child: Container(
                width: 250,
                // padding: EdgeInsets.all(60),
                child: FlatButton(
                  color: maincolor,
                  child: Container(
                      padding: EdgeInsets.all(20),
                      child: Row(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(
                                Icons.archive,
                                color: Colors.white,
                              )),
                          Text(
                            "Compress and export",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      )),
                  onPressed: () {
                    showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text("Information"),
                                  content: Text("Sorry, this function supposed to compress all resources and storyboard to zip and save, so that it can be opened in computer. But I have no time."),
                                  actions: <Widget>[

                                    FlatButton(
                                      child: Text('Ok'),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    )
                                  ],
                                );
                              });
                  },
                )),
          ),
        ],
      ),
    ),
  );
}
