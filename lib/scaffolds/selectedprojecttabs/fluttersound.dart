import 'dart:async';
import 'dart:io';

import 'package:flutter_sound/flutter_sound.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:Proyekto/database.dart';
import 'package:Proyekto/globalvariables.dart';
import 'package:path_provider/path_provider.dart';

bool isRecording = false;
bool isPlaying = false;
StreamSubscription _recorderSubscription;
StreamSubscription _dbPeakSubscription;
StreamSubscription _playerSubscription;
FlutterSound flutterSound;

String recorderTxt = '00:00:00';
String playerTxt = '00:00:00';
double dbLevel;

double slider_current_position = 0.0;
double max_duration = 0.0;

String currentfilename;

void soundinitialize() {
  flutterSound = new FlutterSound();
  flutterSound.setSubscriptionDuration(0.01);
  flutterSound.setDbPeakLevelUpdate(0.8);
  flutterSound.setDbLevelEnabled(true);
  initializeDateFormatting();
}

void startRecorder() async {
  Directory appDocDir = await getApplicationDocumentsDirectory();
  String appDocPath = appDocDir.path;
  currentfilename = appDocPath +
      "/" +
      "R_" +
      new DateTime.now().millisecondsSinceEpoch.toString();
  try {
    String path = await flutterSound.startRecorder(currentfilename + ".m4a",
        bitRate: 44100, numChannels: 1);
    print('startRecorder: $path');

    _recorderSubscription = flutterSound.onRecorderStateChanged.listen((e) {
      DateTime date = new DateTime.fromMillisecondsSinceEpoch(
          e.currentPosition.toInt(),
          isUtc: true);
      String txt = DateFormat('mm:ss', 'en_GB').format(date);

      recorderTxt = txt.substring(0, 5);
      refresh();
    });
    _dbPeakSubscription = flutterSound.onRecorderDbPeakChanged.listen((value) {
      print("got update -> $value");
      dbLevel = value;

      refresh();
    });

    isRecording = true;
    refresh();
  } catch (err) {
    print('startRecorder error: $err');
    isRecording = false;
    refresh();
  }
}

void stopRecorder({String projectid}) async {
  try {
    String result = await flutterSound.stopRecorder();
    print('stopRecorder: $result');

    if (_recorderSubscription != null) {
      _recorderSubscription.cancel();
      _recorderSubscription = null;
    }
    if (_dbPeakSubscription != null) {
      _dbPeakSubscription.cancel();
      _dbPeakSubscription = null;
    }
    isRecording = false;
    await addresource(
        projectid: projectid,
        filelink: currentfilename,
        name: currentfilename.split("/")[currentfilename.split("/").length - 1],
        resourcetype: "recording");
    refresh();
  } catch (err) {
    print('stopRecorder error: $err');
  }
}

void startPlayer({String filename}) async {
  String path = await flutterSound.startPlayer(filename + ".m4a");
  print(filename);
  await flutterSound.setVolume(1.0);
  print('startPlayer: $path');

  try {
    _playerSubscription = flutterSound.onPlayerStateChanged.listen((e) {
      if (e != null) {
        slider_current_position = e.currentPosition;
        max_duration = e.duration;

        DateTime date = new DateTime.fromMillisecondsSinceEpoch(
            e.currentPosition.toInt(),
            isUtc: true);
        String txt = DateFormat('mm:ss:SS', 'en_GB').format(date);

        isPlaying = true;
        playerTxt = txt.substring(0, 8);

        refresh();
      }
    });
  } catch (err) {
    print('error: $err');
  }
}

void stopPlayer() async {
  try {
    String result = await flutterSound.stopPlayer();
    print('stopPlayer: $result');
    if (_playerSubscription != null) {
      _playerSubscription.cancel();
      _playerSubscription = null;
    }

    isPlaying = false;
    refresh();
  } catch (err) {
    print('error: $err');
  }
}

void pausePlayer() async {
  String result = await flutterSound.pausePlayer();
  print('pausePlayer: $result');
}

void resumePlayer() async {
  String result = await flutterSound.resumePlayer();
  print('resumePlayer: $result');
}

void seekToPlayer(int milliSecs) async {
  int secs = Platform.isIOS ? milliSecs / 1000 : milliSecs;

  String result = await flutterSound.seekToPlayer(secs);
  print('seekToPlayer: $result');
}
