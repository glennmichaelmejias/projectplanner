import 'package:flutter/material.dart';
import 'package:flutter_list_drag_and_drop/drag_and_drop_list.dart';
import 'package:Proyekto/database.dart';
import 'package:Proyekto/globalvariables.dart';

bool isaddstory = false;
final txtstoryname = TextEditingController();

Widget tabstoryboard({int index, BuildContext context}) {
  return Scaffold(
      body: Container(
        // padding: EdgeInsets.only(bottom:15),
        child: DragAndDropList<String>(
          storyname,
          itemBuilder: (BuildContext context, item) {
 
            // print(theindex);
            return Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                        left: 15, top: 15, right: 15, bottom: 0),
                    decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(color: Colors.black12, blurRadius: 16)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: ListTile(
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(child: Text(item.split("<midseparator>")[0])),
                          Icon(Icons.drag_handle)
                        ],
                      ),
                    ),
                  ),
                ),
                isdeleting
                    ? IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          showDialog(
                              context: context,
                              barrierDismissible: true,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text("Delete story?"),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text('Yes'),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        deletestory(projectid: dataid[index],storyid: item.split("<midseparator>")[item.split("<midseparator>").length-1]);
                                        refresh();
                                      },
                                    ),
                                    FlatButton(
                                      child: Text('No'),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    )
                                  ],
                                );
                              });
                        },
                      )
                    : Container()
              ],
            );
          },
          onDragFinish: (before, after) {
            updatestoryboardoarder(
                projectid: dataid[index], after: after, before: before);

            refresh();
          },
          canBeDraggedTo: (one, two) => true,
          dragElevation: 20.0,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          isaddstory = true;
          refresh();
        },
      ),
      bottomNavigationBar: isaddstory
          ? Container(
              // height: 100,
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                  border: Border(top: BorderSide(color: Colors.grey[300]))),
              width: double.infinity,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(bottom: 20, top: 15),
                      padding: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: maincolor.withOpacity(0.2)))),
                      child: TextFormField(
                        controller: txtstoryname,
                        maxLines: 10,
                        autofocus: true,
                        onFieldSubmitted: (term) async {},
                        decoration:
                            InputDecoration.collapsed(hintText: "Add story.."),
                      ),
                    ),
                  ),
                  Container(
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.check),
                          onPressed: () async {
                            await addstory(
                                projectid: dataid[index],
                                name: txtstoryname.text);
                            isaddstory = false;
                            txtstoryname.text = "";
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.insert_link),
                          onPressed: () async {},
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          : Container(
              height: 0,
            ));
}
