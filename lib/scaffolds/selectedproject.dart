import 'package:flutter/material.dart';
import 'package:Proyekto/database.dart';
import 'package:Proyekto/globalvariables.dart';
import 'package:Proyekto/scaffolds/selectedprojecttabs/overview.dart';
import 'package:Proyekto/scaffolds/selectedprojecttabs/resources.dart';
import 'package:Proyekto/scaffolds/selectedprojecttabs/storyboard.dart';

int currenttabindex;

void selectedproject({BuildContext context, int index, Function refresh}) {
  gettheresources(database, projectid: dataid[index]);
  getstoryboard(database, projectid: dataid[index]);

  Navigator.of(context).push(PageRouteBuilder(
    opaque: false,
    transitionsBuilder: (
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child,
    ) {
      return SlideTransition(
        position: new Tween<Offset>(
          begin: const Offset(0.0, 1.0),
          end: Offset.zero,
        ).animate(CurvedAnimation(parent: animation, curve: Curves.ease)),
        child: child,
      );
    },
    pageBuilder: (BuildContext context, Animation animation,
            Animation secondaryAnimation) =>
        DefaultTabController(
          length: 3,
          child: Scaffold(
            appBar: AppBar(
              title: Text("  " + dataname[index]),
              bottom: TabBar(
                onTap: (index2) {
                  currenttabindex = index2;
                  isdeleting = false;
                  refresh();
                },
                tabs: [
                  Tab(
                    text: "Overview",
                  ),
                  Tab(
                    text: "Storyboard",
                  ),
                  Tab(
                    text: "Resources",
                  ),
                ],
              ),
              actions: <Widget>[
                !isdeleting
                    ? IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          if (currenttabindex != 0) {
                            isdeleting = true;
                            refresh();
                          } else {
                            showDialog(
                                context: context,
                                barrierDismissible: true,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Warning!"),
                                    content: Text("Are you sure you want to delete project? No undo."),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text('Yes'),
                                        onPressed: () {
                                          Navigator.pop(context);
                                           Navigator.pop(context);
                                          deleteproject(projectid: dataid[index]);
                                          // deleteresource(
                                          //     projectid: projectid,
                                          //     resourceid: resourceid[index]);
                                          // deletestory(projectid: dataid[index],storyid: item.split("<midseparator>")[item.split("<midseparator>").length-1]);
                                          refresh();
                                        },
                                      ),
                                      FlatButton(
                                        child: Text('No'),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                      )
                    : IconButton(
                        icon: Icon(Icons.done),
                        onPressed: () {
                          isdeleting = false;
                          refresh();
                        },
                      ),
              ],
            ),
            body: TabBarView(
              children: [
                taboverview(index: index,context: context),
                tabstoryboard(index: index, context: context),
                tabresources(context: context, index: index, refresh: refresh),
              ],
            ),
          ),
        ),
  ));
}
