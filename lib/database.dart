import 'package:path/path.dart';
import 'package:Proyekto/globalvariables.dart';
import 'package:sqflite/sqflite.dart';
import 'package:intl/intl.dart';

Database database;
Future initDatabase() async {
  var databasesPath = await getDatabasesPath();
  String path = join(databasesPath, 'db.db');

  database = await openDatabase(path, version: 1,
      onCreate: (Database db, int version) async {
    db.execute(
        'CREATE TABLE IF NOT EXISTS projects (id INTEGER PRIMARY KEY, name TEXT, description TEXT, datecreated DATETIME, icon TEXT)');

    db.execute(
        'CREATE TABLE IF NOT EXISTS resources (id INTEGER PRIMARY KEY, name TEXT, filelink TEXT, resourcetype TEXT, datecreated DATETIME, projectid TEXT)');

    db.execute(
        'CREATE TABLE IF NOT EXISTS storyboard(id INTEGER PRIMARY KEY, name TEXT, resourceid TEXT, projectid TEXT)');
  }, onOpen: (Database db) {
    getprojects(db);
  });
}

Future addstory({String name, String resourceid, String projectid}) async {
  // String theorder = new DateTime.now().millisecondsSinceEpoch.toString();
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'INSERT INTO storyboard(name,resourceid,projectid) VALUES("$name","$resourceid","$projectid")');

    getstoryboard(database, projectid: projectid);
  });
}
Future deletestory({String storyid,String projectid}) async {
  // String theorder = new DateTime.now().millisecondsSinceEpoch.toString();
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'DELETE FROM storyboard where id="$storyid"');

    getstoryboard(database, projectid: projectid);
  });
}

Future getstoryboard(thedb, {String projectid}) async {
  List<Map> list = await thedb.rawQuery(
      'SELECT id,name,resourceid,projectid FROM storyboard where projectid="$projectid" order by id');

  storyname.clear();
  storyid.clear();
  storyresourceid.clear();
  storyprojectid.clear();

  list.forEach((f) {
    storyname.add(f['name']+"<midseparator>"+f['id'].toString());
    storyid.add(f['id'].toString());
    storyresourceid.add(f['resourceid']);
    storyprojectid.add(f['projectid'].toString());
  });
  refresh();
}

Future updatestoryboardoarder({int before, int after, String projectid}) async {
  String strstoryname = storyname[before];
  String strstoryresourceid = storyresourceid[before];
  String strstoryprojectid = storyprojectid[before];

  storyname.removeAt(before);
  storyresourceid.removeAt(before);
  storyprojectid.removeAt(before);

  storyname.insert(after, strstoryname);
  storyresourceid.insert(after, strstoryresourceid);
  storyprojectid.insert(after, strstoryprojectid);

  await database.transaction((txn) async {
    for (int a = 0; a < storyid.length; a++) {
      strstoryname = storyname[a];
      strstoryprojectid = storyprojectid[a];

      String tempstrid = storyid[a];
      await txn.rawInsert(
          'UPDATE storyboard set name="$strstoryname" where id=$tempstrid');

      await txn.rawInsert(
          'UPDATE storyboard set projectid="$strstoryprojectid" where id=$tempstrid');
    }

    // print(
    //     'UPDATE storyboard set theorder="$neworder" where theorder="$beforeorder"');

    // print(
    //     'UPDATE storyboard set theorder="$beforeorder" where theorder="$neworder"');

    getstoryboard(database, projectid: projectid);
  });
}

Future createnewproject({String name, String description, String icon}) async {
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'INSERT INTO projects(name,description,icon,datecreated) VALUES("$name","$description","$icon",CURRENT_TIMESTAMP)');

    getprojects(database);
  });
}

Future deleteproject({String projectid}) async {
  // String theorder = new DateTime.now().millisecondsSinceEpoch.toString();
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'DELETE FROM projects where id="$projectid"');

        await txn.rawInsert(
        'DELETE FROM resources where projectid="$projectid"');

        await txn.rawInsert(
        'DELETE FROM storyboard where projectid="$projectid"');

    getprojects(database);
  });
}

Future addresource(
    {String name,
    String filelink,
    String resourcetype,
    String projectid}) async {
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'INSERT INTO resources(name,filelink,resourcetype,datecreated,projectid) VALUES("$name","$filelink","$resourcetype",CURRENT_TIMESTAMP,"$projectid")');
    gettheresources(database, projectid: projectid);
  });
}
Future deleteresource({String resourceid,String projectid}) async {
  // String theorder = new DateTime.now().millisecondsSinceEpoch.toString();
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'DELETE FROM resources where id="$resourceid"');

    gettheresources(database, projectid: projectid);
  });
}
Future editprojectdetails(
    {String name, String description, String icon, String editingid}) async {
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'UPDATE projects set name="$name",description="$description",icon="$icon" where id=$editingid');
    getprojects(database);
  });
}

Future gettheresources(thedb, {String projectid}) async {
  List<Map> list = await thedb.rawQuery(
      'SELECT id,name,filelink,resourcetype,datetime(datecreated,\'localtime\') as datecreated,projectid FROM resources where projectid="$projectid" order by datecreated desc');
  resourcename.clear();
  resourcecreated.clear();
  resourcefilelink.clear();
  resourceid.clear();
  resourcetype.clear();
  resourceprojectid.clear();

  list.forEach((f) {
    resourcename.add(f['name']);
    resourceid.add(f['id'].toString());
    resourcefilelink.add(f['filelink']);
    resourcetype.add(f['resourcetype'].toString());
    resourceprojectid.add(f['projectid']);

    // print(f);
    String daterecorded = f['datecreated'];
    String formattedDate =
        DateFormat('MMM dd, yyy hh:mm aa').format(DateTime.parse(daterecorded));

    resourcecreated.add(formattedDate);
  });
  refresh();
}

Future getprojects(thedb) async {
  List<Map> list = await thedb.rawQuery(
      'SELECT id,name,description,icon,datetime(datecreated,\'localtime\') as datecreated FROM projects order by datecreated ' + (projectorderdesc?"desc":"asc"));
  dataname.clear();
  datacreated.clear();
  datadescription.clear();
  dataicon.clear();
  dataid.clear();

  list.forEach((f) {
    dataname.add(f['name']);
    datadescription.add(f['description']);
    dataicon.add(f['icon']);
    dataid.add(f['id'].toString());

    String daterecorded = f['datecreated'];
    String formattedDate =
        DateFormat('MMM dd, yyy hh:mm aa').format(DateTime.parse(daterecorded));

    datacreated.add(formattedDate);
  });
  refresh();
}
