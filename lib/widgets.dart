import 'package:flutter/material.dart';
import 'package:Proyekto/globalvariables.dart';
import 'package:Proyekto/scaffolds/createproject.dart';

Widget projectslistviewitem(
    {int index,
    Function onlistviewtap,
    BuildContext context,
    Function onsave,
    Function refresh}) {
  return Container(
    margin: EdgeInsets.only(bottom: 10),
    decoration: BoxDecoration(boxShadow: <BoxShadow>[
      BoxShadow(color: Colors.black12, blurRadius: 16)
    ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onlistviewtap,
        child: Container(
          margin: EdgeInsets.only(left: 0, top: 1, right: 0),
          padding: EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 10),
          child: Row(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Icon(iconimage[int.parse(dataicon[index])])),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      dataname[index],
                      style: TextStyle(color: maincolor, fontSize: 16),
                    ),
                    Text(
                      datadescription[index],
                      style: TextStyle(color: Colors.grey),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    )
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () {
                      //listviewitem edit button
                      createproject(
                          context: context,
                          isedit: true,
                          editingindex: index,
                          onsave: onsave,
                          refresh: refresh);
                    },
                  )),
            ],
          ),
        ),
      ),
    ),
  );
}

List<Widget> iconcategory({Function refresh}) {
  List<Widget> temp = List();
  for (int a = 0; a < icontext.length; a++) {
    temp.add(Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          currentselectedcategory = a;
          refresh(a);
        },
        child: Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(10),
          // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
          child: Column(
            children: <Widget>[
              Icon(
                iconimage[a],
                color: currentselectedcategory == a ? maincolor : Colors.grey,
              ),
              Text(
                icontext[a],
                style: TextStyle(
                    color:
                        currentselectedcategory == a ? maincolor : Colors.grey),
              ),
              // currentselectedcategory==a?Icon(Icons.check):Text(""),
            ],
          ),
        ),
      ),
    ));
  }
  return temp;
}
