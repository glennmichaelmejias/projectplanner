import 'package:flutter/material.dart';

Color maincolor = Colors.blueGrey;

List<String> dataname = new List<String>();
List<String> datacreated = new List<String>();
List<String> dataicon = new List<String>();
List<String> datadescription = new List<String>();
List<String> dataid = new List<String>();

List<String> resourcename = new List<String>();
List<String> resourcecreated = new List<String>();
List<String> resourceid = new List<String>();
List<String> resourcetype = new List<String>();
List<String> resourcefilelink = new List<String>();
List<String> resourceprojectid = new List<String>();

List<String> storyname = new List<String>();
List<String> storyresourceid = new List<String>();
List<String> storyid = new List<String>();
List<String> storyprojectid = new List<String>();

TextStyle textstyleflatbutton = TextStyle(color: maincolor);

List<String> icontext = ["Project", "Mobile App", "Website", "Software", "Game", "Audio Production", "Video Production", "Photography", "Animation"];
List<IconData> iconimage = [Icons.insert_drive_file, Icons.phone_android, Icons.web, Icons.computer, Icons.games, Icons.music_note, Icons.music_video, Icons.camera_alt, Icons.movie];
int currentselectedcategory = 0;


Function refresh;
bool isdeleting=false;

bool projectorderdesc=true;
